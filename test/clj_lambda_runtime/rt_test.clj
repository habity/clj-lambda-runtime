(ns clj-lambda-runtime.rt-test
  (:require [clojure.test :as t]
            [cognitect.aws.client.api :as aws]
            [clojure.data.json :as json]
            [clojure.java.io :as io]))

(def function-name (System/getenv "FUNCTION_NAME"))

(defn set-env [client function-name env]
  (aws/invoke client {:op :UpdateFunctionConfiguration
                      :request {:FunctionName  function-name
                                :Environment {:Variables env}}}))

(t/deftest integration-test
  (let [client (aws/client {:api :lambda})
        invoke-request {:FunctionName function-name,
                        :InvocationType "RequestResponse",
                        :LogType "None",
                        :Payload (json/write-str {:test "payload"
                                                  :with [{:nested "map"}]})}]
    (t/testing "Successful invocation"
      (set-env client function-name {})
      (let [res (aws/invoke client {:op :Invoke
                                    :request invoke-request})
            payload (-> res
                        :Payload
                        io/reader
                        (json/read :key-fn keyword))
            context (-> payload
                        :context-str
                        clojure.edn/read-string)]
        (t/is (nil? (:FunctionError res)))
        (t/is (= {:test "payload"
                  :with [{:nested "map"}]}
                 (:event payload)))
        (t/is (clojure.set/subset? #{:lambda-runtime/deadline-ms
                                     :lambda-runtime/invoked-function-arn
                                     :lambda-runtime/aws-request-id
                                     :lambda-runtime/trace-id}
                                   (set (keys context))))))

    (t/testing "Handler failure"
      (set-env client function-name {"FAIL_HANDLER" "1"})
      (let [res (aws/invoke client {:op :Invoke
                                    :request invoke-request})
            payload (-> res
                        :Payload
                        io/reader
                        (json/read :key-fn keyword))]
        (t/is (= "Unhandled" (:FunctionError res)))
        (t/is (= {:errorType "FunctionHandlerException"
                  :errorMessage "Error handling request."}
                 payload))))

    (t/testing "Init failure"
      (set-env client function-name {"FAIL_INIT" "1"})
      (let [res (aws/invoke client {:op :Invoke
                                    :request invoke-request})
            payload (-> res
                        :Payload
                        io/reader
                        (json/read :key-fn keyword))]
        (t/is (= "Unhandled" (:FunctionError res)))
        (t/is (= {:errorMessage "Failed to initialize handler."
                  :errorType "FunctionInitializationException"}
                 payload))))))
