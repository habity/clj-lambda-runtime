(ns clj-lambda-runtime.handler
  (:require [clj-lambda-runtime.rt :as rt])
  (:gen-class))

(defn init []
  (when (System/getenv "FAIL_INIT")
    (throw (RuntimeException. "Initialization failure")))
  {:init :context})

(defn echo-handler [event context]
  (when (System/getenv "FAIL_HANDLER")
    (throw (RuntimeException. "Handler failure")))
  {:event event
   :context context
   ;; JSON-encoding strips namespaces. Encode as str to ease testing.
   :context-str (str context)})

(defn -main []
  (rt/run echo-handler init))
