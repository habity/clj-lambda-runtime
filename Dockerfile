FROM clojure:openjdk-8-tools-deps-1.10.0.414

WORKDIR /opt

ENV GRAALVM_VERSION 1.0.0-rc13
ENV GRAALVM_EDITION ce
ENV GRAALVM_HOME /opt/graalvm-${GRAALVM_EDITION}/${GRAALVM_VERSION} 

RUN wget -O - https://github.com/oracle/graal/releases/download/vm-${GRAALVM_VERSION}/graalvm-${GRAALVM_EDITION}-${GRAALVM_VERSION}-linux-amd64.tar.gz | tar xzf -

ENV GRAALVM_HOME /opt/graalvm-${GRAALVM_EDITION}-${GRAALVM_VERSION} 
RUN cp $JAVA_HOME/jre/lib/security/cacerts $GRAALVM_HOME/jre/lib/security/cacerts

ENV PATH ${PATH}:${GRAALVM_HOME}/bin

RUN apt update && apt install -y build-essential libcurl4-openssl-dev zlib1g-dev zip && apt clean

RUN wget -O /usr/local/bin/lein https://raw.githubusercontent.com/technomancy/leiningen/2.9.1/bin/lein && chmod +x /usr/local/bin/lein && lein --version

WORKDIR /tmp

# Deps caching
ADD project.clj project.clj
ADD deps.edn deps.edn
RUN lein deps

ADD . .

RUN lein native-image && mv target/bootstrap .

RUN cp $JAVA_HOME/jre/lib/amd64/libsunec.so .
RUN zip bootstrap.zip bootstrap libsunec.so

CMD tar cf - bootstrap.zip