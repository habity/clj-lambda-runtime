(ns clj-lambda-runtime.rt
  (:require [clojure.core.async :as a]
            [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.logging :as log]
            [cognitect.http-client :as http])
  (:import (org.apache.log4j MDC)))

(def base-uri "/2018-06-01/runtime")

(defn- get-api [] (System/getenv "AWS_LAMBDA_RUNTIME_API"))

(defn- request [method path]
  (let [[host port] (str/split (get-api) #":")]
    {:uri (str base-uri path)
     :server-name host
     :server-port (Integer. ^String port)
     :scheme "http"
     :request-method method}))

(defprotocol JsonDeserializable
  (deserialize [_]))

(extend-protocol JsonDeserializable
  java.nio.ByteBuffer
  (deserialize [x] (deserialize (.array ^java.nio.ByteBuffer x)))

  java.lang.String
  (deserialize [x] (deserialize (java.io.StringReader. x)))

  java.io.Reader
  (deserialize [x] (json/read x :key-fn keyword))

  nil
  (deserialize [x] x))

(defprotocol JsonSerializable
  (->body [_]))

(extend-protocol JsonSerializable
  java.lang.String
  (->body [x] (->body (.getBytes ^String (json/write-str x))))

  clojure.lang.IPersistentCollection
  (->body [x] (->body (.getBytes ^String (json/write-str x))))

  nil
  (->body [x] x))

(extend (Class/forName "[B")
  JsonDeserializable
  {:deserialize #(deserialize (io/reader %))}

  JsonSerializable
  {:->body #(java.nio.ByteBuffer/wrap %)})

(defn- error [type message]
  {:errorType type
   :errorMessage message})

(defn- ->context [headers]
  (into {}
        (comp (filter (fn [[k v]]
                        (str/starts-with? k "lambda-runtime")))
              (map (fn [[k v]]
                     [(keyword "lambda-runtime" (subs k 15)) v]) ))
        headers))

(defn- exit [exit-code]
  (System/exit exit-code))

(defn- do-get [client path]
  (client (request :get path)))

(defn- get-next [client]
  (let [response (do-get client "/invocation/next")]
    [(-> response :body deserialize)
     (->context (:headers response))]))

(defn- post [client path body]
  (let [request (-> (request :post path)
                    (assoc :headers {"content-type" "application/json"})
                    (assoc :body (->body body)))]
    (client request)))

(defn- init-fail [client error]
  (post client "/init/error" error)
  (exit 1))

(defn- fail [client context error]
  (let [aws-request-id (:lambda-runtime/aws-request-id context)]
    (post client
          (format "/invocation/%s/error" aws-request-id)
          error)))

(defn- success [client context response]
  (let [aws-request-id (:lambda-runtime/aws-request-id context)]
    (post client
          (format "/invocation/%s/response" aws-request-id)
          response)))

(defn- init [client handler-init]
  (try
    (handler-init)
    (catch Exception _
      (init-fail client (error "FunctionInitializationException"
                               "Failed to initialize handler.")))))

(defn- invoke [client handler event context]
  (MDC/put "AWSRequestId" (:lambda-runtime/aws-request-id context))
  (try
    (success client context (handler event context))
    (catch Exception _
      (fail client context (error "FunctionHandlerException"
                                  "Error handling request.")))))

(defn- poll [client handler init-ctx]
  (while true
    (let [[event context] (get-next client)]
      (invoke client handler event (conj context init-ctx)))))

(defn- wrap-exit-on-500 [client]
  (fn [req]
    (let [res (client req)]
      (when (= 500 (:status res))
        (log/error "Received 500 response, shutting down.")
        (exit 1))
      res)))

(defn- wrap-log-on-4xx [client]
  (fn [req]
    (let [res (client req)]
      (when (< 399 (:status res) 500)
        (log/error "4xx received" {:req req :res res}))
      res)))

(defn- wrap-debug [client]
  (if (System/getenv "DEBUG_RUNTIME_API")
    (fn [req]
      (log/info "Making request:" req)
      (let [res (client req)]
        (log/info "Received response:" res)
        res))
    client))

(defn- wrap-exit-on-http-error [client]
  (fn [req]
    (let [res (client req)]
      (if (:cognitect.anomalies/category res)
        (exit 1)
        res))))

(defn- mk-client []
  (let [http-client (http/create {})]
    (-> (fn [req] (a/<!! (http/submit http-client req)))
        wrap-debug
        wrap-exit-on-http-error
        wrap-log-on-4xx
        wrap-exit-on-500)))

(defn run
  ([handler]
   (run handler nil))
  ([handler handler-init]
   (let [client (mk-client)
         init-ctx (when handler-init (init client handler-init))]
     (poll client handler init-ctx))))
