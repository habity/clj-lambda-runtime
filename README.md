# clj-lambda-runtime

## Rationale

Given the slow startup time for any Clojure program it is not a really good fit for AWS Lambda unless you can live with the occasional multi-second response time during cold starts. This project aims for cold start response times below 500ms.

## Approach

Implement a [custom lambda runtime](https://docs.aws.amazon.com/lambda/latest/dg/runtimes-custom.html) in Clojure and build a [GraalVM native-image](https://www.graalvm.org/docs/reference-manual/aot-compilation/) that can be deployed on AWS Lambda.

## Usage

### Project setup

Put this in your deps.edn
```clojure
{:deps {habity/clj-lambda-runtime {:git/url "https://bitbucket.org/habity/clj-lambda-runtime"
                                   :sha "5a0f9362604477f72a840bf2da3dbc52a94e9e15"}}
```

And this in your project.clj
```clojure
:plugins [[lein-tools-deps "0.4.3"]
          [io.taylorwood/lein-native-image "0.3.0"]]
:middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]
:lein-tools-deps/config {:config-files [:install :user :project]}
:main my-superfast.handler
:native-image
{:name "bootstrap"
 :opts ["--enable-https"
        "-Djava.net.preferIPv4Stack=true"
        "-H:ReflectionConfigurationResources=clj-lambda-custom-runtime-reflection-config.json"
        "-H:CPUFeatures=AES,AVX,CMOV,CX8,ERMS,FXSR,MMX,POPCNT,SSE4_1,SSE4_2,SSSE3,TSC"]}
```

### Handler

Write your handler like this

```clojure
(ns my-superfast.handler
  (:require [clj-lambda-runtime.rt :as rt])
  (:gen-class))

(defn init 
  "Creates initial context that will be passed to the handler.
  Do setup of db-connections, http-clients etc here and return them in a map."
  []
  {:init :context})

(defn echo-handler 
  "Handler function that is invoked by the runtime. Receives an event as a map
  and the context as initialized by init merged with the following keys from the runtime:
  
  :lambda-runtime/aws-request-id
  :lambda-runtime/deadline-ms
  :lambda-runtime/invoked-function-arn
  :lambda-runtime/trace-id"
  [event context]
  {:event event})

(defn -main 
  "Pass control over to the runtime"
  []
  (rt/run echo-handler init))
```

### Building on Linux (preferrably on EC2 using Amazon Linux)

Requires having JAVA_HOME pointing to an OpenJDK/Oracle Java JDK installation and GRAALVM_HOME pointing to your GraalVM installation.

```sh
cp $JAVA_HOME/jre/lib/security/cacerts $GRAALVM_HOME/jre/lib/security/cacerts
lein native-image
cd target
cp $JAVA_HOME/jre/lib/amd64/libsunec.so .
zip bootstrap.zip bootstrap libsunec.so

```

### Building using Docker

```sh
docker build -t clj-lambda-runtime .
docker run --rm clj-lambda-custom-runtime | tar -xf - -C target
```

### Create your lambda

```sh
aws lambda create-function --function-name clj-on-lambda-rocks --runtime provided --handler ignored --role arn-for-a-valid-role --memory-size 128 --zip-file fileb://target/bootstrap.zip
```

### Invoke it (output formatted for easier reading)

```sh
aws lambda invoke --log-type Tail --payload '{"the": "request"}' --function-name clj-on-lambda-rocks /dev/stderr | jq '.LogResult | @base64d'
{"event":{"the":"request"}}
START RequestId: 911670f0-36fc-43cb-a2d1-70b266c1bd60 Version: $LATEST
END RequestId: 911670f0-36fc-43cb-a2d1-70b266c1bd60
REPORT RequestId: 911670f0-36fc-43cb-a2d1-70b266c1bd60 Init Duration: 166.07 ms Duration: 97.51 ms Billed Duration: 300 ms  Memory Size: 128 MB Max Memory Used: 65 MB \n"
```

Yes, that is correct: Clojure cold start in less than 500ms.

### Debugging

If you experience something unusual you can set the `DEBUG_RUNTIME_API` environment variable on your function and you will see debug output of all interactions with the AWS Runtime API.

## Bugs/Limitations

* Currently does not propagate the \_X\_AMZN\_TRACE\_ID environment variable, breaking X-Ray.
* Lambda-Runtime-Client-Context and Lambda-Runtime-Cognito-Identity are not passed in the context as maps.
