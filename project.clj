(defproject clj-lambda-runtime "0.1.0-SNAPSHOT"
  :plugins [[lein-tools-deps "0.4.3"]
            [io.taylorwood/lein-native-image "0.3.0"]]
  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]
  :lein-tools-deps/config {:config-files [:install :user :project]}

  :license {:name "The MIT License"
            :url "http://opensource.org/licenses/MIT"
            :distribution :repo}

  :native-image
  {:name "bootstrap"
   :opts ["--enable-https"
          "-Djava.net.preferIPv4Stack=true"
          "-H:ReflectionConfigurationResources=clj-lambda-custom-runtime-reflection-config.json"
          "-H:CPUFeatures=AES,AVX,CMOV,CX8,ERMS,FXSR,MMX,POPCNT,SSE4_1,SSE4_2,SSSE3,TSC"]}
  :profiles
  {:dev
   {:dependencies [[com.cognitect.aws/api "0.8.273"]
                   [com.cognitect.aws/endpoints "1.1.11.507"]
                   [com.cognitect.aws/lambda "705.2.402.0"]]
    :source-paths ["src" "test"]
    :main clj-lambda-runtime.handler}}
  :jvm-opts ["-Dclojure.compiler.direct-linking=true"])

